Genshi>=0.5
lxml>=2.0

[chart]
pycha>=0.4.0
pyyaml

[fodt]
python-magic
